# coding: utf-8
from sqlalchemy import Column, DECIMAL, Date, ForeignKey, String
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

import pymongo

## MYSQL PART
Base = declarative_base()
engine = create_engine("mysql://root:@localhost/music", echo=True)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)

## MONGO ATLAS PART
client = pymongo.MongoClient("mongo_connexion_url")


class Client(Base):
    __tablename__ = "clients"

    cli_id = Column(INTEGER(5), primary_key=True)
    cli_nom = Column(String(30), nullable=False)
    cli_adr = Column(String(50), nullable=False)
    cli_cps = Column(String(5), nullable=False)
    cli_ville = Column(String(30), nullable=False)
    cli_email = Column(String(50), nullable=False)
    cli_dnai = Column(Date, nullable=False)

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


class Genre(Base):
    __tablename__ = "genres"

    gen_genre = Column(String(3), primary_key=True)
    gen_libelle = Column(String(20), nullable=False)

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


class Instrument(Base):
    __tablename__ = "instruments"

    ins_cod = Column(String(3), primary_key=True)
    ins_libelle = Column(String(30), nullable=False)

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


class Pay(Base):
    __tablename__ = "pays"

    pay_pays = Column(String(3), primary_key=True)
    pay_libelle = Column(String(20), nullable=False)

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


class Artiste(Base):
    __tablename__ = "artistes"

    art_id = Column(INTEGER(5), primary_key=True)
    art_nom = Column(String(30), nullable=False)
    art_typ = Column(String(1), nullable=False)
    art_pays = Column(ForeignKey("pays.pay_pays"), nullable=False, index=True)
    art_genre = Column(ForeignKey("genres.gen_genre"), nullable=False, index=True)

    genre = relationship("Genre")
    pay = relationship("Pay")

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


class Membre(Base):
    __tablename__ = "membres"

    mem_id = Column(INTEGER(5), primary_key=True)
    mem_nom = Column(String(30), nullable=False)
    mem_pays = Column(ForeignKey("pays.pay_pays"), nullable=False, index=True)
    mem_ins = Column(ForeignKey("instruments.ins_cod"), nullable=False, index=True)

    instrument = relationship("Instrument")
    pay = relationship("Pay")

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


class Album(Base):
    __tablename__ = "albums"

    alb_id = Column(INTEGER(5), primary_key=True)
    alb_art = Column(ForeignKey("artistes.art_id"), nullable=False, index=True)
    alb_nom = Column(String(50), nullable=False)
    alb_annee = Column(INTEGER(4), nullable=False)
    alb_prix = Column(DECIMAL(5, 2), nullable=False)
    alb_couv = Column(String(250))

    artiste = relationship("Artiste")

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


class Historique(Base):
    __tablename__ = "historique"

    his_id = Column(INTEGER(5), primary_key=True)
    his_art = Column(ForeignKey("artistes.art_id"), nullable=False, index=True)
    his_mem = Column(ForeignKey("membres.mem_id"), nullable=False, index=True)
    his_debut = Column(INTEGER(4), nullable=False)
    his_fin = Column(INTEGER(4), nullable=False)

    artiste = relationship("Artiste")
    membre = relationship("Membre")

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


class Vente(Base):
    __tablename__ = "ventes"

    ven_id = Column(INTEGER(5), primary_key=True)
    ven_date = Column(Date, nullable=False)
    ven_alb = Column(ForeignKey("albums.alb_id"), nullable=False, index=True)
    ven_cli = Column(ForeignKey("clients.cli_id"), nullable=False, index=True)
    ven_prix = Column(DECIMAL(5, 2), nullable=False)

    album = relationship("Album")
    client = relationship("Client")

    def __repr__(self):
        return str(self.__class__) + ": " + str(self.__dict__)


def generate_ventes_collection():
    ven_col = []
    ventes = session.query(Vente).all()

    ## VENTES PART
    for v in ventes:
        ventes_dict = {
            "date": v.ven_date.strftime("%d/%m/%Y"),
            "prix": float(v.ven_prix),
        }

        ## CLIENT PART
        client = session.query(Client).filter_by(cli_id=v.ven_cli).one()

        cli_dict = {
            "nom": client.cli_nom,
            "adresse": client.cli_adr,
            "cps": client.cli_cps,
            "ville": client.cli_ville,
            "email": client.cli_email,
            "date_naissance": client.cli_dnai.strftime("%d/%m/%Y"),
        }
        ventes_dict["client"] = cli_dict

        ## ALBUM
        album = session.query(Album).filter_by(alb_id=v.ven_alb).one()

        albm_dict = {
            "nom": album.alb_nom,
            "annee": album.alb_annee,
            "prix": float(album.alb_prix),
        }
        ventes_dict["album"] = albm_dict
        ven_col.append(ventes_dict)
    return ven_col


def generate_artiste_collection():

    art_col = []
    artistes = session.query(Artiste).all()

    for a in artistes:

        ## ARTISTES PART
        artistes_dict = {
            "nom": a.art_nom,
            "type_musique": a.art_typ,
            "pays": {"pays_code": a.pay.pay_pays, "pays_libelle": a.pay.pay_libelle},
            "genre": {
                "genre_code": a.genre.gen_genre,
                "genre_libelle": a.genre.gen_libelle,
            },
        }

        ## ALBUMS ARTIST PART
        albums = session.query(Album).filter_by(alb_art=a.art_id).all()

        art_albs_list = []

        for alb in albums:
            alb_dict = {
                "nom": alb.alb_nom,
                "annee": alb.alb_annee,
                "prix": float(alb.alb_prix),
                "couverture": "/static/images/{album_name}.jpg".format(
                    album_name=alb.alb_nom.replace(" ", "")
                ),
            }
            art_albs_list.append(alb_dict)

        artistes_dict["albums"] = art_albs_list

        ## MEMBRES ARTIST PART
        historiques = session.query(Historique).filter_by(his_art=a.art_id).all()

        art_mem_list = []

        for his in historiques:
            mem_dict = {
                "nom": his.membre.mem_nom,
                "pays": his.membre.mem_pays,
                "date_debut": his.his_debut,
                "date_fin": his.his_fin,
                "instruments": {
                    "code": his.membre.instrument.ins_cod,
                    "libelle": his.membre.instrument.ins_libelle,
                },
            }
            art_mem_list.append(mem_dict)
        artistes_dict["membres"] = art_mem_list
        art_col.append(artistes_dict)

    return art_col


if __name__ == "__main__":
    session = Session()

    # Creating jsons from mysql db
    artiste_collection = generate_artiste_collection()
    vente_collection = generate_ventes_collection()

    # Getting mongo db and cols
    mongo_db = client["musicdb-migrated"]
    mongo_db["artiste"].drop()
    mongo_db["vente"].drop()
    mongo_artiste_col = mongo_db["artiste"]
    mongo_vente_col = mongo_db["vente"]

    # Inserting mongo jsons
    mongo_artiste_col.insert_many(artiste_collection)
    mongo_vente_col.insert_many(vente_collection)
