import pymongo
import os

from pprint import pprint
from jinja2 import Environment, FileSystemLoader

PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_ENVIRONMENT = Environment(
    autoescape=False,
    loader=FileSystemLoader(os.path.join(PATH, "templates")),
    trim_blocks=False,
)


def render_template(template_filename, context):
    return TEMPLATE_ENVIRONMENT.get_template(template_filename).render(context)


## MONGO ATLAS PART
client = pymongo.MongoClient("mongo_connexion_url")

mongo_db = client["musicdb-migrated"]
mongo_artiste_col = mongo_db["artiste"]
mongo_vente_col = mongo_db["vente"]

## Affichage d'une liste des albums
data = mongo_artiste_col.find({}, {"nom": 1, "albums.nom": 1, "albums.annee": 1})
albums = []
for d in data:
    for album in d["albums"]:
        albums.append(
            {"artiste": d["nom"], "album": album["nom"], "annee": album["annee"]}
        )
question_un = sorted(albums, key=lambda album: album["annee"])
print("***************************************************")


## Affichage d'une liste des artistes
data = mongo_artiste_col.find(
    {}, {"nom": 1, "pays.pays_libelle": 1, "genre.genre_libelle": 1}
)
artistes = []
for d in data:
    artistes.append(
        {
            "nom": d["nom"],
            "genre": d["genre"]["genre_libelle"],
            "pays": d["pays"]["pays_libelle"],
        }
    )
question_deux = sorted(artistes, key=lambda artiste: artiste["nom"])
print("***************************************************")


## Affichage d'un catalogue
data = mongo_artiste_col.find(
    {}, {"nom": 1, "albums.nom": 1, "albums.prix": 1, "albums.couverture": 1}
)
albums = []
for d in data:
    for album in d["albums"]:
        albums.append(
            {
                "artiste": d["nom"],
                "album": album["nom"],
                "prix": album["prix"],
                "couverture": album["couverture"],
            }
        )
question_trois = sorted(albums, key=lambda album: (album["prix"], album["album"]))
pprint("***************************************************")


def render_queries():
    fname = "queries.html"
    context = {
        "question_un": question_un,
        "question_deux": question_deux,
        "question_trois": question_trois,
    }
    with open(fname, "w") as f:
        html = render_template("template.html", context)
        f.write(html)


if __name__ == "__main__":
    render_queries()
